#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <limits>
#include <array>
#include <functional>
#include <map>
#include <filesystem>

#include "Step.h"
//#include "HK.h"
#include "toolbox.h"

#include <TROOT.h>
#include <TFile.h>

using namespace std;
using namespace filesystem;

const vector<double> pt_edges { 74, 84, 97, 114, 133, 153, 174, 196, 220, 245, 272, 300, 330, 362, 395, 430, 468, 507, 548, 592, 638, 686, 737, 790, 846, 905, 967, 1032, 1101, 1172, 1248, 1327, 1410, 1497, 1588, 1784, 2116, 2500};
const int nPtBins = pt_edges.size()-1;

const vector<string> systs {"Unfolding", "AbsStat", "AbsScale", "AbsMPFB", "Fragm", "SiPionE", "SiPionH", "FlavorQCD", "RelJEREC1", "RelJEREC2", "RelJERHF", "RelPtBB", "RelPtEC1", "RelPtEC2", "RelPtHF", "RelFSR", "RelStaEC2", "RelStaHF", "PU_DataMC", "PU_PtRef", "PU_PtBB", "PU_PtEC1", "PU_PtEC2", "PU_PtHF", "RelStaFSR"};

int main (int argc, char * argv[])
{
    int maxdegree = argc > 1 ? atoi(argv[1]) : 7;
    double stopParam = argc > 2 ? atof(argv[2]) : 0.90;
    UInt_t nrep = argc > 3 ? atoi(argv[3]) : 10000;
    cout << "maxdegree = " << maxdegree << '\n'
         << "stopParam = " << stopParam << '\n'
         << "nrep = " << nrep << endl;

    //Step::verbose = true;
    cout << "ROOT version: " << gROOT->GetVersion() << endl;
    TH1::SetDefaultSumw2(true);

    array<array<vector<vector<double>>,6>,6> statcovariances;
    array<             vector<double> ,6   > differences;
    array<map<string  ,vector<double>>, 6> systUps, systDns;
    int ndfTot = 0;

    TH2 * hP8 = nullptr;
    path pP8 = "CMS_P8.root";
    if (exists(pP8)) {
        auto fP8 = TFile::Open(pP8.c_str(), "READ");
        hP8 = dynamic_cast<TH2*>(fP8->Get("h2"));
        assert(hP8 != nullptr);
        hP8->SetDirectory(0);
        fP8->Close();
    }

#ifdef STEP_HK
    TFile fOut("CMS_HK.root", "RECREATE");
#else
    TFile fOut("CMS.root", "RECREATE");
#endif
    Table table {"CMS.txt"}, tableProb {"CMSprob.txt"}, tableAsimov {"CMS_P8.txt"};
    ofstream fileFtest, fileXval;
    fileFtest.open("CMS_Ftest.txt");
    if (nrep > 0)
        fileXval.open("CMS_Xval.txt");
    for (int y = 1; y <= 6; ++y) {
        fOut.cd();

        cout << "--------------------------- y = " << y << '\n';

        auto dir = fOut.mkdir(Form("ybin%d", y));
        dir->cd();

        auto h = new TH1D("h", ";Jet p_{T}   [GeV];#frac{d^{2}#sigma}{dp_{T}dy}   [pb/GeV]", nPtBins, pt_edges.data());
        auto corr = new TH2D("corr", ";Jet p_{T}   [GeV];Jet p_{T}   [GeV];", nPtBins, pt_edges.data(), nPtBins, pt_edges.data());

        for (auto& syst: systs) {
            systUps[y-1][syst].reserve(nPtBins);
            systDns[y-1][syst].reserve(nPtBins);
        }

        cout << "Reading tables" << endl;
        ifstream fIn;
        fIn.open(Form("tables/CMS_8TeV_jets_Ybin%d.dat",y));
        while (fIn.good()) {
            int lowedge; double xsec, stat;
            fIn >> lowedge >> xsec >> stat;
            if (fIn.eof()) break;
            int i = h->FindBin(lowedge+1);
            h->SetBinContent(i, xsec);
            h->SetBinError  (i, stat);
            for (auto& syst: systs) {
                double up, down;
                fIn >> up >> down;
                systUps[y-1][syst].push_back(xsec*up);
                systDns[y-1][syst].push_back(xsec*down);
            }
        }
        fIn.close();

        int N = h->GetNbinsX();
        while (std::abs(h->GetBinContent(N)) < Step::eps) { --N; assert(N > 1); }

        fIn.open(Form("tables/CMS_8TeV_jets_Ybin%d.corr",y));
        while (fIn.good()) {
            int lowedge1, lowedge2; double content;
            fIn >> lowedge1 >> lowedge2 >> content;
            if (fIn.eof()) break;
            int i = corr->GetXaxis()->FindBin(lowedge1+1);
            int j = corr->GetYaxis()->FindBin(lowedge2+1);
            corr->SetBinContent(i, j, content);
        }
        fIn.close();

        auto cov = (TH2 *) corr->Clone("cov");
        statcovariances[y-1][y-1] = vector<vector<double>> ( N,
                                           vector<double>   (N, 0));
        for (int x1 = 1; x1 <= N; ++x1)
        for (int x2 = 1; x2 <= N; ++x2) {
            auto content = corr->GetBinContent(x1,x2);

            auto s1 = h->GetBinError(x1),
                 s2 = h->GetBinError(x2);

            auto sigsig = content*s1*s2;
            if (x1 == x2) sigsig += pow(0.01 * h->GetBinContent(x1),2);

            if (s1*s2 > 0) cov->SetBinContent(x1,x2,sigsig);
            statcovariances[y-1][y-1][x1-1][x2-1] = sigsig;
        }

        auto hExt = (TH1 *) h->Clone("hExt");
        for (int x = 1; x <= N; ++x) {
            auto content = hExt->GetBinContent(x),
                 error   = hExt->GetBinError  (x);
            hExt->SetBinError(x,hypot(error,0.01*content));
        }
        h->GetYaxis()->SetTitle("#frac{d^{2}#sigma}{dp_{T}dy} / Step( #frac{d^{2}#sigma}{dp_{T}dy} )");
        hExt->GetYaxis()->SetTitle(h->GetYaxis()->GetTitle());

#ifdef STEP_HK
        TF1 * f = Step::GetSmoothFitHK(hExt, cov, 1, N, 8000, stopParam, cout);
#else
        TF1 * f = Step::GetSmoothFit<Step::Basis::Chebyshev,log,exp>(hExt, cov, 1, N, maxdegree,
                                            Step::EarlyStopping::xVal, stopParam, nrep, cout);
#endif
        table(Step::chi2s);
        tableProb[Step::chi2s];

        corr->Scale(100);
        corr->SetMaximum( 100);
        corr->SetMinimum(-100);

        h->Write();
        hExt->Write();
        corr->Write();
        f->Write("f");

#ifndef STEP_HK
        fileFtest << "ybin = " << y << '\n';
        PrintAllTests(Step::Ftest, fileFtest, Step::bestResult->degree());
        fileFtest << "----\n";
        if (nrep > 0) {
            fileXval << "ybin = " << y << '\n';
            PrintAllTests(Step::Xval, fileXval, Step::bestResult->degree());
            fileXval << "----\n";
        }
        WriteChi2s(Step::chi2s);
#endif
        WritePulls(hExt, f, N);

        TH1 * smooth = GetSmoothHist(h,f,N);
        smooth->Write("smooth");

#ifndef STEP_HK
        // Asimov
        if (hP8 == nullptr) {
            cout << "No Asimov test will be run" << endl;
            continue;
        }
        else {
            cout << "Running on Asimov data" << endl;
            TH1 * hAsimov = hP8->ProjectionX(Form("Asimov_%d", y), y, y);
            hAsimov->Scale(1, "width");
            double integral = hAsimov->Integral();
            assert(integral > 0);
            hAsimov->Scale(hExt->Integral()/hAsimov->Integral());
            int deg = f->GetNpar()-1;
            TF1 * fAsimov = Step::GetSmoothFit<Step::Basis::Chebyshev,log,exp>(hAsimov, cov, 1, N, deg);
            TH1 * sAsimov = GetSmoothHist(hAsimov,fAsimov,N);
            hAsimov->Divide(sAsimov);
            TString title = fAsimov->GetTitle();
            title.ReplaceAll(Form("_{%d}", deg), "_{A}");
            hAsimov->SetTitle(title);
            hAsimov->Write("Asimov");
            tableAsimov(Step::chi2s);
        }
#endif

        differences[y-1].reserve(N);
        for (int i = 1; i <= N; ++i) {
            double x = h->GetBinContent(i),
                   fx = smooth->GetBinContent(i);
            differences[y-1].push_back(x - fx);
        }

        h->Divide(smooth);
        h->SetTitle(""); // TODO
        h->Write("ratio");

        hExt->Divide(smooth);
        hExt->SetTitle(f->GetTitle());
        hExt->Write("ratioExt");

        ndfTot += Step::bestResult->ndf;

        delete h;
        delete hExt;
        delete f;
        delete smooth;
        delete cov;
        delete corr;
    }
    fileFtest.close();
    if (nrep)
        fileXval.close();
    fOut.Close();

    for (size_t y1 = 0; y1 < 6; ++y1)
    for (size_t y2 = 0; y2 < 6; ++y2) 
        if (y1 != y2) statcovariances[y1][y2] = vector<vector<double>>( differences[y1].size(),
                                                       vector<double>  (differences[y2].size(),0.));

    int Ntot = accumulate(begin(differences), end(differences), 0,
            [](int N, vector<double>& difference) { return N + difference.size(); } );
    TMatrixD cov(Ntot,Ntot);
    int n1 = 0;
    for (size_t y1 = 0; y1 < 6; ++y1)
    for (size_t i1 = 0; i1 < differences[y1].size(); ++i1) {
        int n2 = 0;
        for (size_t y2 = 0; y2 < 6; ++y2)
        for (size_t i2 = 0; i2 < differences[y2].size(); ++i2) {
            cov(n1,n2) = statcovariances[y1][y2][i1][i2];
            for (auto& sys: systs) {
                const auto& sysUp1 = systUps[y1][sys],
                            sysDn1 = systDns[y1][sys],
                            sysUp2 = systUps[y2][sys],
                            sysDn2 = systDns[y2][sys];
                cov(n1,n2) += 0.5*(sysUp1[i1]*sysUp2[i2]
                                  +sysDn1[i1]*sysDn2[i2]);
            }
            ++n2;
        }
        ++n1;
    }

    TDecompSVD svd(cov);
    svd.SetTol(Step::eps);
    TVectorD v = svd.GetSig();
    bool status = false;
    cov = svd.Invert(status);
    assert(status);

    double chi2 = 0;
    n1 = 0;
    for (size_t y1 = 0; y1 < 6; ++y1)
    for (size_t i1 = 0; i1 < differences[y1].size(); ++i1) {
        int n2 = 0;
        for (size_t y2 = 0; y2 < 6; ++y2)
        for (size_t i2 = 0; i2 < differences[y2].size(); ++i2) {
            double diff1 = differences[y1][i1];
            double diff2 = differences[y2][i2];
            double sig = cov(n1,n2);
            if (std::abs(sig) > 0) chi2 += diff1*diff2*sig;
            ++n2;
        }
        ++n1;
    }
    cout << "chi2/ndf = " << chi2 << '/' << ndfTot << " = " << chi2/ndfTot << " (prob = " << TMath::Prob(chi2, ndfTot) << ")" << endl;

    return EXIT_SUCCESS;
}
