#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <limits>
#include <filesystem>

#include "Step.h"
//#include "HK.h"
#include "toolbox.h"

#include <TROOT.h>
#include <TFile.h>

using namespace std;
using namespace filesystem;

const vector<double> pt_edges { 62, 72, 83, 96, 110, 127, 146, 169, 195, 224, 259, 298, 344, 396, 457, 527, 700 };
const int nPtBins = pt_edges.size()-1;

int main (int argc, char * argv[])
{
    int maxdegree = argc > 1 ? atoi(argv[1]) : 5;
    double stopParam = argc > 2 ? atof(argv[2]) : 0.90;
    UInt_t nrep = argc > 3 ? atoi(argv[3]) : 10000;
    cout << "maxdegree = " << maxdegree << '\n'
         << "stopParam = " << stopParam << '\n'
         << "nrep = " << nrep << endl;

    //Step::verbose = true;
    cout << "ROOT version: " << gROOT->GetVersion() << endl;
    TH1::SetDefaultSumw2(true);

    TH2 * hP8 = nullptr;
    path pP8 = "CDF_P8.root";
    if (exists(pP8)) {
        auto fP8 = TFile::Open(pP8.c_str(), "READ");
        hP8 = dynamic_cast<TH2*>(fP8->Get("h2"));
        assert(hP8 != nullptr);
        hP8->SetDirectory(0);
        fP8->Close();
    }

#ifdef STEP_HK
    auto fOut = TFile::Open("CDF_HK.root", "RECREATE");
#else
    auto fOut = TFile::Open("CDF.root", "RECREATE");
#endif
    Table table {"CDF.txt"}, tableProb {"CDFprob.txt"}, tableAsimov {"CDF_P8.txt"};
    ofstream fileFtest, fileXval;
    fileFtest.open("CDF_Ftest.txt");
    if (nrep > 0)
        fileXval.open("CDF_Xval.txt");
    for (int y = 1; y <= 5; ++y) {
        fOut->cd();

        cout << "--------------------------- y = " << y << '\n';

        auto dir = fOut->mkdir(Form("ybin%d", y));
        dir->cd();

        auto h = new TH1D("h", ";Jet p_{T}   [GeV];#frac{d^{2}#sigma}{dp_{T}dy}   [pb/GeV]", nPtBins, pt_edges.data());

        ifstream fIn;
        fIn.open(Form("tables/CDF_1960GeV_jets_Ybin%d.dat",y));
        while (fIn.good()) {
            double center, xsec, stat;
            fIn >> center >> xsec >> stat;
            if (fIn.eof()) break;
            int i = h->FindBin(center);
            h->SetBinContent(i, xsec);
            h->SetBinError  (i, stat);
        }
        fIn.close();

        int N = h->GetNbinsX();
        while (std::abs(h->GetBinContent(N)) < Step::eps) { --N; assert(N > 1); }
#ifdef STEP_HK
        TF1 * f = Step::GetSmoothFitHK(h, 1, N, 1960, stopParam, cout);
#else
        TF1 * f = Step::GetSmoothFit<Step::Basis::Chebyshev,log,exp>(h, 1, N, maxdegree,
                                            Step::EarlyStopping::xVal, stopParam, nrep, cout);
#endif
        table(Step::chi2s);
        tableProb[Step::chi2s];

        h->Write();
        f->Write("f");

#ifndef STEP_HK
        fileFtest << "ybin = " << y << '\n';
        PrintAllTests(Step::Ftest, fileFtest, Step::bestResult->degree());
        fileFtest << "----\n";
        if (nrep > 0) {
            fileXval << "ybin = " << y << '\n';
            PrintAllTests(Step::Xval, fileXval, Step::bestResult->degree());
            fileXval << "----\n";
        }
        WriteChi2s(Step::chi2s);
#endif
        WritePulls(h, f, N);

        TH1 * smooth = GetSmoothHist(h,f,N);
        smooth->Write("smooth");

#ifndef STEP_HK
        // Asimov
        if (hP8 == nullptr) {
            cout << "No Asimov test will be run" << endl;
            continue;
        }
        else {
            cout << "Running on Asimov data" << endl;
            TH1 * hAsimov = hP8->ProjectionX(Form("Asimov_%d", y), y, y);
            hAsimov->Scale(1, "width");
            double integral = hAsimov->Integral();
            assert(integral > 0);
            hAsimov->Scale(h->Integral()/hAsimov->Integral());
            for (int i = 1; i <= N; ++i) {
                double error = h->GetBinError(i);
                hAsimov->SetBinError(i, error);
            }
            int deg = f->GetNpar()-1;
            TF1 * fAsimov = Step::GetSmoothFit<Step::Basis::Chebyshev,log,exp>(hAsimov, 1, N, deg);
            TH1 * sAsimov = GetSmoothHist(hAsimov,fAsimov,N);
            hAsimov->Divide(sAsimov);
            TString title = fAsimov->GetTitle();
            title.ReplaceAll(Form("_{%d}", deg), "_{A}");
            hAsimov->SetTitle(title);
            hAsimov->Write("Asimov");
            tableAsimov(Step::chi2s);
        }
#endif

        h->Divide(smooth);
        h->SetTitle(f->GetTitle());
        h->Write("ratio");

        delete h;
        delete f;
        delete smooth;
    }
    fileFtest.close();
    if (nrep)
        fileXval.close();
    fOut->Close();

    return EXIT_SUCCESS;
}
