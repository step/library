#ifndef TOOLBOX
#define TOOLBOX

#include <map>
#include <vector>

#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>

#include <TProfile.h>

struct Table {
    const char * fname;
    std::map<int,std::stringstream> rows;

    void operator() (const std::deque<Step::Result>& chi2s)
    {
        using namespace std;
        for (auto chi2: Step::chi2s) {
            size_t d = chi2.degree();
            rows[d] << fixed << setprecision(2)
                    << setw(10) << chi2.x2n() << "\u00B1"
                    << setw(4) << chi2.x2nErr();
        }
    }

    void operator[] (const std::deque<Step::Result>& chi2s)
    {
        using namespace std;
        for (auto chi2: Step::chi2s) {
            size_t d = chi2.degree();
            rows[d] << fixed << setprecision(2)
                    << setw(10) << chi2.prob();
        }
    }


    ~Table ()
    {
        using namespace std;

        ofstream output;
        output.open(fname);
        cout << "---------------------------\n";
        for (auto& row: rows) {
            output << setw(3) << row.first << row.second.str() << '\n';
            cout   << setw(3) << row.first << row.second.str() << '\n';
        }
        output.close();
    }
};

void WritePulls (TH1 * h, TF1 * f, int N)
{
    auto residuals = dynamic_cast<TH1*>(h->Clone("residuals"));
    residuals->GetYaxis()->SetTitle("#frac{#sigma - smooth(#sigma)}{#delta}");
    residuals->Reset();
    auto pulls = new TH1D("pulls", ";#frac{#sigma - smooth(#sigma)}{#delta};N", 10, -2.5, 2.5),
         relDiffs = new TH1D("relDiffs", ";100 #times #left( frac{#sigma_{i}}{smooth(#sigma_{i}} - 1 #right);N", 10, -2.5, 2.5);
    for (int i = 1; i <= N; ++i) {
        double content = h->GetBinContent(i),
               x1 = h->GetBinLowEdge(i),
               x2 = h->GetBinLowEdge(i+1),
               smooth = f->Integral(x1,x2)/(x2-x1),
               error = h->GetBinError(i);
        if (std::abs(error) < Step::eps) continue;
        double pull = (content - smooth)/error,
               relDiff = 100 * ( content / smooth - 1);
        residuals->SetBinContent(i, pull);
        pulls->Fill(pull);
        relDiffs->Fill(relDiff);
    }
    relDiffs->Write();
    pulls->Write();
    residuals->Write();
}

void WriteChi2s (const std::deque<Step::Result>& chi2s)
{
    using namespace std;

    size_t n = chi2s.size();
    auto h_chi2  = new TH1D("chi2" , ";degree;#chi^{2}/ndf", n, 0.5, 0.5+n);
    auto p_chi2T = new TProfile("chi2T", ";degree;#chi^{2}/ndf", n, 0.5, 0.5+n),
         p_chi2V = new TProfile("chi2V", ";degree;#chi^{2}/ndf", n, 0.5, 0.5+n);
    for (size_t i = 0; i < n; ++i) {
        const auto& chi2 = chi2s.at(i);
        h_chi2 ->SetBinContent(i+1, chi2.x2n   ());
        h_chi2 ->SetBinError  (i+1, chi2.x2nErr());

        assert(chi2.X.size() == i+2);
        const int ndf = chi2.ndf;
        for (auto& chi2T: chi2.chi2sT) p_chi2T->Fill(i+1, chi2T/ ndf     );
        for (auto& chi2V: chi2.chi2sV) p_chi2V->Fill(i+1, chi2V/(ndf+i+2));
    }
    h_chi2 ->Write();
    p_chi2T->Write();
    p_chi2V->Write();
}

TH1 * GetSmoothHist (TH1 * h, TF1 * f, int N)
{
    auto h2 = dynamic_cast<TH1*>(h->Clone(Form("h_%s", f->GetName())));
    h2->Reset();

    for (int i = 1; i <= N; ++i) {

        double x1 = h->GetBinLowEdge(i),
               x2 = h->GetBinLowEdge(i+1);
        double content = f->Integral(x1,x2)/(x2-x1);

        h2->SetBinContent(i, content);
    }
    
    return h2;
}

#endif
