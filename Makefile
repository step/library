ROOTINC    = $(shell root-config --incdir)
ROOTCFLAGS = $(shell root-config --cflags)
ROOTLIBS   = $(shell root-config --libs)

CFLAGS=-g -Wall -O3 -std=c++17 -fPIC -fconcepts -DDEBUG

all: CDF D0 CMS ATLAS 

%: %.cc *.h
	g++ $(CFLAGS) $(ROOTCFLAGS) $< $(ROOTLIBS) -lMathCore -o $@

clean: 
	@rm -f CMS CDF ATLAS D0 *.so

.PHONY: all clean
