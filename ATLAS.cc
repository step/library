#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <limits>
#include <filesystem>
#include <array>
#include <functional>
#include <map>

#include "Step.h"
//#include "HK.h"
#include "toolbox.h"

#include <TROOT.h>
#include <TFile.h>

using namespace std;
using namespace filesystem;

static const vector<double> pt_edges {70, 85, 100, 116, 134, 152, 172, 194, 216, 240, 264, 290, 318, 346, 376, 408, 442, 478, 516, 556, 598, 642, 688, 736, 786, 838, 894, 952, 1012, 1076, 1162, 1310, 1530, 1992, 2500};
static const int nPtBins = pt_edges.size()-1;
static const size_t Nobs = 10000;
static const vector<int> uncorSys { 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255, 256, 257, 258, 259, 260, 261, 262, 263, 264, 270, 271, 272, 273, 274, 275, 276, 277, 278, 279, 280, 281, 282, 293, 294, 295, 296, 297, 298, 299, 300, 301, 302, 316, 317, 318, 319, 320, 321, 322, 323, 324, 325, 326};

vector<vector<double>> GetReplicas (int Nbins, int eta)
{
    vector<double> tpl(Nbins); // template to book the memory
    vector<vector<double>> replicas(Nobs, tpl);
    ifstream f;
    TString bucket;
    for (size_t i = 0; i < replicas.size(); ++i) {
        path fname = Form("HEPdata/heprep%zu_R06_Eta%d.txt", i, eta);
        if (!exists(fname)) {
            cerr << "\x1B[31m\e[1mUse the `download` script in the `HEPdata` subdirectory to get the HEP record of the ATLAS data.\nAborting.\x1B[30m\e[0m\n";
            exit(EXIT_FAILURE);
        }
        f.open(fname.c_str());

        // skip header
        while (bucket != "xlow")
            f >> bucket;
        f >> bucket >> bucket;

        // get replica
        for (double& v: replicas[i]) {
            assert(f.good());
            // ptmin ptmax content
            f >> v >> v >> v;
            // (we only need the content)
        }
        f.close();
    }
    return replicas;
}

vector<vector<double>> GetVariations (vector<vector<double>> replicas)
{
    assert(replicas.size() == Nobs);
    size_t Nbins = replicas.front().size();

    vector<double> means(Nbins, 0);

    // get mean
    for (size_t j = 0; j < Nbins; ++j) {
        for (const auto& replica: replicas) 
            means[j] += replica[j];
        means[j] /= Nobs;
    }

    // subtract mean
    for (size_t j = 0; j < Nbins; ++j)
        for (auto& replica: replicas) 
            replica[j] -= means[j];

    return replicas;
}

vector<vector<double>> GetCovariance (const vector<vector<double>>& variations1,
                                      const vector<vector<double>>& variations2)
{
    assert(variations1.size() == Nobs);
    assert(variations2.size() == Nobs);

    size_t Nbins1 = variations1.front().size(),
           Nbins2 = variations2.front().size();
    vector<vector<double>> covariance(Nbins1,
           vector<double>            (Nbins2, 0));

    if (variations1 == variations2)
    for (size_t j = 0; j < Nbins1; ++j) 
    for (size_t k = 0; k < Nbins2; ++k) {
        for (size_t i = 0; i < Nobs; ++i)
            covariance[j][k] += variations1[i][j] * variations2[i][k];
        covariance[j][k] /= Nobs;
    }

    return covariance;
}

vector<vector<double>> GetCovariance (const vector<vector<double>>& variations)
{
    return GetCovariance(variations, variations);
}

[[ deprecated ]]
vector<vector<double>> GetCovariance (size_t Nbins, int y)
{
    auto replicas = GetReplicas(Nbins, y);

    vector<double> means(Nbins,0);
    vector<vector<double>> covariance(Nbins, means);

    // get mean
    for (size_t j = 0; j < Nbins; ++j) {
        for (const auto& replica: replicas) 
            means[j] += replica[j];
        means[j] /= Nobs;
    }

    // subtract mean
    for (size_t j = 0; j < Nbins; ++j)
        for (auto& replica: replicas) 
            replica[j] -= means[j];

    // calculate covariance
    for (size_t j = 0; j < Nbins; ++j) 
    for (size_t k = 0; k < Nbins; ++k) {
        for (const auto& replica: replicas)
            covariance[j][k] += replica[j] * replica[k];
        covariance[j][k] /= Nobs;
    }

    return covariance;
}

void WriteCorrelationMatrix (TH2 * cov, int N)
{
    TString name = cov->GetName();
    name.ReplaceAll("cov", "corr");
    auto corr = dynamic_cast<TH2 *>(cov->Clone(name));
    for (int i = 1; i <= N; ++i) 
    for (int j = 1; j <= N; ++j) {
        auto content = cov->GetBinContent(i,j) / sqrt(cov->GetBinContent(i,i) * cov->GetBinContent(j,j));
        corr->SetBinContent(i,j,content);
    }
    corr->GetZaxis()->SetTitle("[%]");
    corr->Scale(100);
    corr->SetMinimum(-100);
    corr->SetMaximum( 100);
    corr->Write();
    delete corr;
}

int main (int argc, char * argv[])
{
    int maxdegree = argc > 1 ? atoi(argv[1]) : 7;
    double stopParam = argc > 2 ? atof(argv[2]) : 0.90;
    bool autostop = argc > 3 ? atoi(argv[3]) : 1;
    UInt_t nrep = argc > 4 ? atoi(argv[4]) : 10000;
    cout << "maxdegree = " << maxdegree << '\n'
         << "stopParam = " << stopParam << '\n'
         << "autostop = " << autostop << '\n'
         << "nrep = " << nrep << endl;

    TH2 * hP8 = nullptr;
    path pP8 = "ATLAS_P8.root";
    if (exists(pP8)) {
        auto fP8 = TFile::Open(pP8.c_str(), "READ");
        hP8 = dynamic_cast<TH2*>(fP8->Get("h2"));
        assert(hP8 != nullptr);
        hP8->SetDirectory(0);
        fP8->Close();
    }

    //Step::verbose = true;
    cout << "ROOT version: " << gROOT->GetVersion() << endl;
    TH1::SetDefaultSumw2(true);

    array<      vector<vector<double>>,6   > variations;
    array<array<vector<vector<double>>,6>,6> statcovariances;
    array<             vector<double> ,6   > differences;
    array<map<int,vector<double>>, 6> systUps, systDns;
    int ndfTot = 0;

    path fname = "HEPdata/HEPData-ins1604271-v1-root.root";
    // this contains only the nominal value as well as the systematic variations, but not replicas
    if (!exists(fname)) {
        cerr << "\x1B[31m\e[1mUse the `download` script in the `HEPdata` subdirectory to get the HEP record of the ATLAS data.\nAborting.\x1B[30m\e[0m\n";
        return EXIT_FAILURE;
    }
    TFile fIn(fname.c_str(), "READ");

#ifdef STEP_HK
    TFile fOut("ATLAS_HK.root", "RECREATE");
#else
    TFile fOut("ATLAS.root", "RECREATE");
#endif
    Table table {"ATLAS.txt"}, tableProb {"ATLASprob.txt"}, tableAsimov {"ATLAS_P8.txt"};
    ofstream fileFtest, fileXval;
    fileFtest.open("ATLAS_Ftest.txt");
    if (nrep > 0)
        fileXval.open("ATLAS_Xval.txt");
    for (int y = 1; y <= 6; ++y) {

        cout << "--------------------------- y = " << y << '\n';

        // getting nominal value
        fIn.cd();
        auto nominal = dynamic_cast<TH1F*>(fIn.Get(Form("Table %d/Hist1D_y1", y)));
        assert(nominal != nullptr);
        nominal->SetTitle(";Jet p_{T}   [GeV];#frac{d^{2}#sigma}{dp_{T}dy}   [pb/GeV]");
        nominal->SetDirectory(0);
        nominal->SetName("nominal");

        cout << "Guessing number of bins (check non-empty bins)" << endl;
        int N = nominal->GetNbinsX();
        while (std::abs(nominal->GetBinContent(N)) < Step::eps) { --N; assert(N > 1); }

        cout << "Getting replicas" << endl;
        const vector<vector<double>> replicas = GetReplicas(N,y);
        variations[y-1] = GetVariations(replicas);

        cout << "Preparing output ROOT file" << endl;
        fOut.cd();
        auto dir = fOut.mkdir(Form("ybin%d", y));
        dir->cd();
        nominal->SetDirectory(dir);

        auto covStat  = new TH2D("covStat" ,";Jet p_{T}   [GeV];Jet p_{T}   [GeV];", nPtBins, pt_edges.data(), nPtBins, pt_edges.data()),
             covSys   = new TH2D("covSys"  ,";Jet p_{T}   [GeV];Jet p_{T}   [GeV];", nPtBins, pt_edges.data(), nPtBins, pt_edges.data()),
             cov      = new TH2D("cov"     ,";Jet p_{T}   [GeV];Jet p_{T}   [GeV];", nPtBins, pt_edges.data(), nPtBins, pt_edges.data());

        cout << "Getting statistical covariance matrix from replicas" << endl;
        statcovariances[y-1][y-1] = GetCovariance(variations[y-1]);

        for (int i = 1; i <= N; ++i) 
        for (int j = 1; j <= N; ++j) 
            covStat->SetBinContent(i,j,statcovariances[y-1][y-1][i-1][j-1]);

        cout << "Getting all systematic variations" << endl;
        for (int sys = 2; sys <= 330; ++sys) {
            auto sysUp = dynamic_cast<TH1F*>(fIn.Get(Form("Table %i/Hist1D_y1_e%dplus" , y, sys))),
                 sysDn = dynamic_cast<TH1F*>(fIn.Get(Form("Table %i/Hist1D_y1_e%dminus", y, sys)));
            systUps[y-1][sys].resize(N);
            systDns[y-1][sys].resize(N);
            for (int i = 1; i <= N; ++i) {
                systUps[y-1][sys][i-1] = sysUp->GetBinContent(i);
                systDns[y-1][sys][i-1] = sysDn->GetBinContent(i);
            }
            delete sysUp;
            delete sysDn;
        }

        cout << "Getting systematic covariance matrix (`*stat*` variations only)" << endl;
        for (int sys: uncorSys) {
            const auto& sysUp = systUps[y-1][sys],
                        sysDn = systUps[y-1][sys];
            for (int i = 1; i <= N; ++i) 
            for (int j = 1; j <= N; ++j) {
                double content = covSys->GetBinContent(i,j) + sysUp[i-1]*sysUp[j-1]
                                                            + sysDn[i-1]*sysDn[j-1];
                assert(!isnan(content));
                assert(isfinite(content));
                covSys->SetBinContent(i,j,content);
            }
        }
        covSys->Scale(0.5); // to symmetrise

        cout << "Getting global covariance matrix" << endl;
        cov->Add(covStat);
        cov->Add(covSys);

        cout << "Adding uncertainties to nominal histogram (only for plotting)" << endl;
        auto nominalExt = dynamic_cast<TH1*>(nominal->Clone("hExt"));
        for (int i = 1; i <= N; ++i) {
            assert(covStat != nullptr);
            double error2  = covStat->GetBinContent(i,i);
            nominal->SetBinError(i,sqrt(error2));

            assert(cov != nullptr);
            error2 = cov->GetBinContent(i,i);
            nominalExt->SetBinError(i,sqrt(error2));
        }

        cout << "Running test of smoothness" << endl;
#ifdef STEP_HK
        TF1 * f = Step::GetSmoothFitHK(nominal, cov, 1, N, 8000, stopParam, cout);
#else
        TF1 * f = Step::GetSmoothFit<Step::Basis::Chebyshev,log,exp>(nominal, cov, 1, N, maxdegree,
                                            Step::EarlyStopping::xVal, stopParam, nrep, cout);
#endif
        table(Step::chi2s);
        tableProb[Step::chi2s];

        cout << "Writing to file" << endl;
        nominal->Write("h");
        nominalExt->Write("hExt");
        f->Write("f");
        covStat->Write();
        covSys->Write();
        cov->Write();

        cout << "Extracting and saving correlation matrices" << endl;
        WriteCorrelationMatrix(covStat, N),
        WriteCorrelationMatrix(covSys , N),
        WriteCorrelationMatrix(cov    , N);

#ifndef STEP_HK
        fileFtest << "ybin = " << y << '\n';
        PrintAllTests(Step::Ftest, fileFtest, Step::bestResult->degree());
        fileFtest << "----\n";
        if (nrep > 0) {
            fileXval << "ybin = " << y << '\n';
            PrintAllTests(Step::Xval, fileXval, Step::bestResult->degree());
            fileXval << "----\n";
        }
        WriteChi2s(Step::chi2s);
#endif
        WritePulls(nominalExt, f, N);

        TH1 * smooth = GetSmoothHist(nominal,f,N);
        smooth->Write("smooth");

        differences[y-1].reserve(N);
        for (int i = 1; i <= N; ++i) {
            double x = nominal->GetBinContent(i),
                   fx = smooth->GetBinContent(i);
            differences[y-1].push_back(x - fx);
        }

#ifndef STEP_HK
        // Asimov
        if (hP8 == nullptr) {
            cout << "No Asimov test will be run" << endl;
            continue;
        }
        else {
            cout << "Running on Asimov data" << endl;
            TH1 * hAsimov = hP8->ProjectionX(Form("Asimov_%d", y), y, y);
            hAsimov->Scale(1, "width");
            double integral = hAsimov->Integral();
            assert(integral > 0);
            hAsimov->Scale(nominalExt->Integral()/hAsimov->Integral());
            int deg = f->GetNpar()-1;
            TF1 * fAsimov = Step::GetSmoothFit<pow,log,exp>(hAsimov, cov, 1, N, deg);
            TH1 * sAsimov = GetSmoothHist(hAsimov,fAsimov,N);
            hAsimov->Divide(sAsimov);
            TString title = fAsimov->GetTitle();
            title.ReplaceAll(Form("_{%d}", deg), "_{A}");
            hAsimov->SetTitle(title);
            hAsimov->Write("Asimov");
            tableAsimov(Step::chi2s);
        }
#endif

        nominal->Divide(smooth);
        nominal->SetTitle(""); // TODO
        nominal->Write("ratio");

        nominalExt->Divide(smooth);
        nominalExt->SetTitle(f->GetTitle());
        nominalExt->Write("ratioExt");

        ndfTot += Step::bestResult->ndf;

        delete nominal;
        delete nominalExt;
        delete f;
        delete smooth;
        delete covStat;
        delete covSys;
        delete cov;
    }
    fileFtest.close();
    if (nrep)
        fileXval.close();
    fOut.Close();

    cout << "=====================\n"
         << "Calculating global chi2 with *all* stat und syst uncertainties" << endl;
    
    for (size_t y1 = 0; y1 < 6; ++y1)
    for (size_t y2 = 0; y2 < 6; ++y2) 
        if (y1 != y2) statcovariances[y1][y2] = GetCovariance(variations[y1], variations[y2]);

    int Ntot = accumulate(begin(differences), end(differences), 0,
            [](int N, vector<double>& difference) { return N + difference.size(); } );
    TMatrixD cov(Ntot,Ntot);
    int n1 = 0;
    for (size_t y1 = 0; y1 < 6; ++y1)
    for (size_t i1 = 0; i1 < differences[y1].size(); ++i1) {
        int n2 = 0;
        for (size_t y2 = 0; y2 < 6; ++y2)
        for (size_t i2 = 0; i2 < differences[y2].size(); ++i2) {
            cov(n1,n2) = statcovariances[y1][y2][i1][i2];
            for (int sys = 2; sys <= 330; ++sys) {
                const auto& sysUp1 = systUps[y1][sys],
                            sysDn1 = systDns[y1][sys],
                            sysUp2 = systUps[y2][sys],
                            sysDn2 = systDns[y2][sys];
                cov(n1,n2) += 0.5*(sysUp1[i1]*sysUp2[i2]
                                  +sysDn1[i1]*sysDn2[i2]);
            }
            ++n2;
        }
        ++n1;
    }

    TDecompSVD svd(cov);
    svd.SetTol(Step::eps);
    TVectorD v = svd.GetSig();
    bool status = false;
    cov = svd.Invert(status);
    assert(status);

    double chi2 = 0;
    n1 = 0;
    for (size_t y1 = 0; y1 < 6; ++y1)
    for (size_t i1 = 0; i1 < differences[y1].size(); ++i1) {
        int n2 = 0;
        for (size_t y2 = 0; y2 < 6; ++y2)
        for (size_t i2 = 0; i2 < differences[y2].size(); ++i2) {
            double diff1 = differences[y1][i1];
            double diff2 = differences[y2][i2];
            double sig = cov(n1,n2);
            if (std::abs(sig) > 0) chi2 += diff1*diff2*sig;
            ++n2;
        }
        ++n1;
    }
    cout << "chi2/ndf = " << chi2 << '/' << ndfTot << " = " << chi2/ndfTot << " (prob = " << TMath::Prob(chi2, ndfTot) << ")" << endl;

    fIn.Close();

    return EXIT_SUCCESS;
}
