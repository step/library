#ifndef STEP_HK
#define STEP_HK

namespace Step {

////////////////////////////////////////////////////////////////////////////////
/// Ansatz proposed by Harris and Kousouris
///
/// R. M. Harris and K. Kousouris, “Searches for dijet resonances at hadron colliders”, Int. J.
/// Mod. Phys. A 26 (2011) 5005, doi:10.1142/S0217751X11054905, arXiv:1110.5302.
struct HarrisKousouris : public FunctionalForm {

    //////////////////////////////////////////////////////////////////////////////// 
    /// Constructor
    HarrisKousouris (double mi, double Ma, std::ostream& Stream = std::cout) :
        FunctionalForm(__func__, /*npars*/ 6, mi, Ma, Stream) { }

    ////////////////////////////////////////////////////////////////////////////////
    /// operator overloading for the functor to be used with ROOT TF1
    double operator() (const double * x, const double * p) const override
    {
        return p[0] * pow(1 - *x/p[4] + p[5]*pow(*x/p[4],2), p[2]               )  // from Eq. 48
                    / pow(    *x                           , p[1] + p[3]*log(*x)); // from Eq. 49
    }
};

////////////////////////////////////////////////////////////////////////////////
/// Alternative smooth fit based for `HarrisKousouris` function.
/// It assumes that the histogram and the covariance matrix are already
/// normalised to the bin width (i.e. cross section, not count).
TF1 * GetSmoothFitHK (TH1 * h, TH2 * cov, int im, int iM, double sqrts,
                        double stopParam = 1, std::ostream& stream = std::cout)
{
    using namespace std;
    assert(h != nullptr);

    if (verbose)
        stream << "im = " << im << ", iM = " << iM << '\n';

    unsigned int nbins = iM-im+1;
    if (verbose)
        stream << "nbins = " << nbins << '\n';
    assert(nbins > 2);

    double m = h->GetBinLowEdge(im),
           M = h->GetBinLowEdge(iM+1);
    if (verbose)
        stream << "m = " << m << ", M = " << M << '\n';

    HarrisKousouris hk(m, M, stream);
    Correlator<HarrisKousouris> cor(h, cov, im, iM, hk, stream);
    ROOT::Math::Functor functor(cor, hk.npars);

    auto minimiser = ROOT::Math::Factory::CreateMinimizer("Minuit2", "Migrad"); // these are the defaults for ROOT TF1 class
    const int Ncalls = 1e6;
    minimiser->SetMaxFunctionCalls(Ncalls);
    minimiser->SetFunction(functor);
    minimiser->SetTolerance(0.001);
    minimiser->SetPrintLevel(verbose);

    double n = 5, n2 = 1; // biased from inclusive jet spectrum
    double N = h->GetBinContent(h->FindBin(M/2))*pow(M,n)*pow(2,n-1);

    double step = 0.0001;
    minimiser->SetVariable       (0,"N"  , N,N/100);
    minimiser->SetLimitedVariable(1,"n"  , n,step, 3, 10);
    minimiser->SetLimitedVariable(2,"pow",n2,step, 0, 20);
    minimiser->SetVariable       (3,"ln" ,0.,step);
    minimiser->SetLimitedVariable(4,"max"  , M+10,step, M+10, 0.5*sqrts); // biased to inclusive jet spectrum 
    minimiser->SetLimitedVariable(5,"extra", 0.,step, 0, 0.5*sqrts);
    minimiser->FixVariable(3);
    minimiser->FixVariable(4);
    minimiser->FixVariable(5);

    minimiser->ReleaseVariable(0);
    minimiser->ReleaseVariable(1);
    minimiser->ReleaseVariable(2);

    // running
    chi2s.clear();
    chi2s.push_back({false, inf, nbins-3, {N,n,n2}});
    auto output = chi2s.begin();

    stream << "Starting point (not a fit):\n3 parameters:\t" << N << '\t' << n << '\t' << n2 << "\n--------\n";

    auto fit = [&]() {

        // reinitialise the value from `output`
        for (size_t i = 0; i < output->X.size(); ++i)
            minimiser->SetVariableValue(i, output->X[i]);

        bool valid = minimiser->Minimize();
        const double * X = minimiser->X();
        assert(X != nullptr);
        double chi2 = cor(X);

        assert(std::isnormal(chi2));
        unsigned int npars = minimiser->NFree(),
                     ndf = nbins - npars;

        Result current {valid, chi2, ndf, std::vector<double>(X,X+npars)};
        switch (minimiser->Status()) {
            case 0:  stream << current; break;
            case 1:  stream << "\x1B[31mWarning: Fit parameter covariance was made positive defined\n\x1B[30m\e[0m"; break;
            case 2:  stream << "\x1B[31mWarning: Hesse is invalid\n\x1B[30m\e[0m"; break;
            case 3:  stream << "\x1B[31mWarning: Expected Distance reached from the Minimum (EDM = " << minimiser->Edm() << ") is above max\n\x1B[30m\e[0m"; break;
            case 4:  stream << "\x1B[31mWarning: Reached call limit (Ncalls = " << Ncalls << ")\n\x1B[30m\e[0m"; break;
            case 5:
            default: stream << "\x1B[31mWarning: Unknown failure\n\x1B[30m\e[0m";
        }

        return current;
    };

    // 1) only 3 parameters
    stream << "Fitting with only three parameters\n";
    chi2s.push_back(fit());
    if (chi2s.back().valid) ++output;
    stream << "--------\n";

    // 2) then try in parallel to add each of the additional parameters
    stream << "Fitting with more freedom on numerator exponent\n";
    minimiser->ReleaseVariable(3);
    Result try3 = fit();
    double ftest3 = -1;
    if (output->valid) {
        if (try3.valid)
            ftest3 = Ftest(*output, try3);
        stream << (ftest3 > stopParam ? green : red) << "F-test: " << ftest3 << def << "\n--------\n";
    }

    stream << "Fitting instead with more freedom in pt normalisation in the bracket expression\n";
    minimiser->FixVariable(3);
    minimiser->ReleaseVariable(4);
    Result try4 = fit();

    double ftest4 = -1;
    if (output->valid) {
        if (try4.valid)
            ftest4 = Ftest(*output, try4);
        stream << (ftest4 > stopParam ? green : red) << "F-test: " << ftest4 << def << "\n--------\n";
    }

    if (output->valid) {
             if (ftest4 > max(ftest3,stopParam)) chi2s.push_back(try4);
        else if (ftest3 > max(ftest4,stopParam)) chi2s.push_back(try3);
    }
    else if (try3.valid && try4.valid) {
        if (abs(try3.x2n() - 1) < abs(try4.x2n()-1)) chi2s.push_back(try3);
        else                                         chi2s.push_back(try4);
    }
    else if (try3.valid) chi2s.push_back(try3);
    else if (try4.valid) chi2s.push_back(try4);

    output = prev(chi2s.end());

    stream << "Fitting with both parameters released\n";
    minimiser->ReleaseVariable(3);
    Result try34 = fit();
    double ftest34 = output->valid && try34.valid ? Ftest(*output, try34) : -1;
    stream << (ftest34 > stopParam ? green : red) << "F-test: " << ftest34 << def << "\n--------\n";

    if (ftest34 > stopParam || (try34.valid && !output->valid)) chi2s.push_back(try34);
    output = prev(chi2s.end());

#ifdef EXTRAPAR
    stream << "Fitting with higher-order parameter released\n";
    minimiser->ReleaseVariable(5);
    Result try5 = fit();
    double ftest5 = output->valid && try5.valid ? Ftest(*output, try5) : -1;
    stream << (ftest5 > stopParam ? green : red) << "F-test: " << ftest5 << def << "\n--------\n";

    if (ftest5 > stopParam || (try5.valid && !output->valid)) chi2s.push_back(try5);
    output = prev(chi2s.end());
#endif

    // TODO: check if any valid fit
    chi2s.pop_front(); // removing first entry, which is not a fit (chi2 = inf)

    hk.npars = output->X.size();
    auto f = new TF1(Form("hk_%s", h->GetName()), hk, m, M, hk.npars);
    f->SetParameters(output->X.data());
    f->SetTitle(Form("#chi^{2}_{HK(%ld)}/ndf = %.1f/%d", output->X.size(), output->chi2, output->ndf));
    return f;
}

TF1 * GetSmoothFitHK (TH1 * h, TH2 * cov, double sqrts,
        double stopParam = 1, std::ostream& stream = std::cout)
{
    int im = 1, iM = h->GetNbinsX();
    return GetSmoothFitHK(h, cov, im, iM, sqrts, stopParam, stream);
}

TF1 * GetSmoothFitHK (TH1 * h, int im, int iM, double sqrts,
        double stopParam = 1, std::ostream& stream = std::cout)
{
    return GetSmoothFitHK(h, nullptr, im, iM, sqrts, stopParam, stream);
}

TF1 * GetSmoothFitHK (TH1 * h, double sqrts,
        double stopParam = 1, std::ostream& stream = std::cout)
{
    return GetSmoothFitHK(h, nullptr, sqrts, stopParam, stream);
}

}
#endif
