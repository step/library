#include <cstdlib>
#include <iostream>
#include <fstream>
#include <limits>
#include <filesystem>

#include "Step.h"
//#include "HK.h"
#include "toolbox.h"

#include <TROOT.h>
#include <TFile.h>

using namespace std;
using namespace filesystem;

const vector<vector<double>> pt_edges {
    { 50, 60, 70, 80, 90, 100, 110, 120, 130, 145, 160, 180, 200, 220, 240, 265, 295, 325, 360, 400, 445, 490, 540, 665 },
    { 50, 60, 70, 80, 90, 100, 110, 120, 130, 145, 160, 180, 200, 220, 240, 265, 295, 325, 360, 400, 445, 495, 635 },
    { 50, 60, 70, 80, 90, 100, 110, 125, 140, 155, 170, 190, 210, 230, 250, 270, 300, 335, 375, 415, 520 },
    { 50, 60, 70, 80, 90, 100, 110, 125, 140, 155, 170, 190, 215, 240, 265, 290, 325, 415 },
    { 50, 60, 70, 80, 90, 100, 110, 125, 140, 160, 175, 190, 210, 235, 260, 320 },
    { 50, 60, 70, 80, 90, 100, 110, 120, 130, 145, 160, 175, 200, 230 }
};

int main (int argc, char * argv[])
{
    int maxdegree = argc > 1 ? atoi(argv[1]) : 6;
    double stopParam = argc > 2 ? atof(argv[2]) : 0.90;
    UInt_t nrep = argc > 3 ? atoi(argv[3]) : 10000;
    cout << "maxdegree = " << maxdegree << '\n'
         << "stopParam = " << stopParam << '\n'
         << "nrep = " << nrep << endl;

    //Step::verbose = true;
    cout << "ROOT version: " << gROOT->GetVersion() << endl;
    TH1::SetDefaultSumw2(true);

    TH2 * hP8 = nullptr;
    path pP8 = "D0_P8.root";
    if (exists(pP8)) {
        auto fP8 = TFile::Open(pP8.c_str(), "READ");
        hP8 = dynamic_cast<TH2*>(fP8->Get("h2"));
        assert(hP8 != nullptr);
        hP8->SetDirectory(0);
        fP8->Close();
    }

#ifdef STEP_HK
    auto fOut = TFile::Open("D0_HK.root", "RECREATE");
#else
    auto fOut = TFile::Open("D0.root", "RECREATE");
#endif
    Table table {"D0.txt"}, tableProb {"D0prob.txt"}, tableAsimov {"D0_P8.txt"};
    ofstream fileFtest, fileXval;
    fileFtest.open("D0_Ftest.txt");
    if (nrep > 0)
        fileXval.open("D0_Xval.txt");
    for (int y = 1; y <= 6; ++y) {
        fOut->cd();

        cout << "--------------------------- y = " << y << '\n';

        auto dir = fOut->mkdir(Form("ybin%d", y));
        dir->cd();

        const int nPtBins = pt_edges.at(y-1).size()-1;
        auto h    = new TH1D("h"   , ";Jet p_{T}   [GeV];#frac{d^{2}#sigma}{dp_{T}dy}   [pb/GeV]", nPtBins, pt_edges.at(y-1).data()),
             hExt = new TH1D("hExt", ";Jet p_{T}   [GeV];#frac{d^{2}#sigma}{dp_{T}dy}   [pb/GeV]", nPtBins, pt_edges.at(y-1).data());

        ifstream fIn;
        fIn.open(Form("tables/D0_1960GeV_jets_Ybin%d.dat",y));
        while (fIn.good()) {
            double lowedge, xsec, stat, uncor;
            fIn >> lowedge >> xsec >> stat >> uncor;
            if (fIn.eof()) break;
            int i = h->FindBin(lowedge+1);
            stat *= xsec/100;
            uncor *= xsec/100;
            h->SetBinContent(i, xsec);
            h->SetBinError  (i, stat);
            hExt->SetBinContent(i, xsec);
            hExt->SetBinError  (i, hypot(stat,uncor));
        }
        fIn.close();

        int N = h->GetNbinsX();
        while (std::abs(h->GetBinContent(N)) < Step::eps) { --N; assert(N > 1); }
#ifdef STEP_HK
        TF1 * f = Step::GetSmoothFitHK(hExt, 1, N, 1960, stopParam, cout);
#else
        TF1 * f = Step::GetSmoothFit<Step::Basis::Chebyshev,log,exp>(hExt, 1, N, maxdegree,
                                        Step::EarlyStopping::xVal, stopParam, nrep, cout);
#endif
        table(Step::chi2s);
        tableProb[Step::chi2s];

        h->Write();
        hExt->Write();
        f->Write("f");

#ifndef STEP_HK
        fileFtest << "ybin = " << y << '\n';
        PrintAllTests(Step::Ftest, fileFtest, Step::bestResult->degree());
        fileFtest << "----\n";
        if (nrep > 0) {
            fileXval << "ybin = " << y << '\n';
            PrintAllTests(Step::Xval, fileXval, Step::bestResult->degree());
            fileXval << "----\n";
        }
        WriteChi2s(Step::chi2s);
#endif
        WritePulls(hExt, f, N);

        TH1 * smooth = GetSmoothHist(h,f,N);
        smooth->Write("smooth");

#ifndef STEP_HK
        // Asimov
        if (hP8 == nullptr) {
            cout << "No Asimov test will be run" << endl;
            continue;
        }
        else {
            cout << "Running on Asimov data" << endl;
            TH1 * hAsimov = hP8->ProjectionX(Form("Asimov_%d", y), y, y);
            hAsimov->Scale(1, "width");
            double integral = hAsimov->Integral();
            assert(integral > 0);
            hAsimov->Scale(hExt->Integral()/hAsimov->Integral());
            for (int i = 1; i <= N; ++i) {
                double error = hExt->GetBinError(i);
                hAsimov->SetBinError(i, error);
            }
            int deg = f->GetNpar()-1;
            TF1 * fAsimov = Step::GetSmoothFit<Step::Basis::Chebyshev,log,exp>(hAsimov, 1, N, deg);
            TH1 * sAsimov = GetSmoothHist(hAsimov,fAsimov,N);
            hAsimov->Divide(sAsimov);
            TString title = fAsimov->GetTitle();
            title.ReplaceAll(Form("_{%d}", deg), "_{A}");
            hAsimov->SetTitle(title);
            hAsimov->Write("Asimov");
            tableAsimov(Step::chi2s);
        }
#endif

        h->Divide(smooth);
        h->SetTitle(""); // TODO
        h->Write("ratio");

        hExt->Divide(smooth);
        hExt->SetTitle(f->GetTitle());
        hExt->Write("ratioExt");

        delete h;
        delete hExt;
        delete f;
        delete smooth;
    }
    fileFtest.close();
    if (nrep)
        fileXval.close();
    fOut->Close();

    return EXIT_SUCCESS;
}
